import * as React from 'react';
import './App.css';
import { Component, SFC } from 'react';
import * as R from 'ramda';

const logo = require('./logo.svg');

interface Note {
  id: string;
  content: string;
}

// Test data
const note1: Note = { id: '1', content: 'First note' };
const note2: Note = { id: '2', content: 'Second note' };
const note3: Note = { id: '3', content: 'Thirds note' };

const initialState = { notes: [note1, note2, note3], currentNote: '' };
type State = Readonly<typeof initialState>;

type NoteProps = { onClick(): void };
const Note: SFC<NoteProps> = ({ onClick: handleClick, children }) => (
  <li className="Note">
    {children}
    <button className="Delete-button" onClick={handleClick}>X</button>
  </li>
);

class Notes extends Component<object, State> {
  readonly state: State = initialState;

  deleteNote = (id: string) => {
    this.setState({
      notes: R.reject((note: Note) => note.id === id, this.state.notes)
    });
  }

  addNote = () => {
    const id = R.toString(Math.round(Math.random() * 100));
    this.setState({
      notes: R.append({
        id: id,
        content: this.state.currentNote
      },
                      this.state.notes),
                      currentNote: ''
    });
  }

  handleChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    this.setState({currentNote: event.target.value});
  }

  render() {
    const notes = this.state.notes.map((note, i) => {
      return (
        <Note key={note.id} onClick={() => { this.deleteNote(note.id); }}>
          {note.content}
        </Note>
      );
    });

    return (
      <div>
        <textarea value={this.state.currentNote} onChange={this.handleChange} />
        <button onClick={this.addNote} disabled={!this.state.currentNote}>
          Add note
        </button>
        <ol>{notes}</ol>
      </div>
    );
  }
}

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Notes App</h1>
        </header>
        <p className="App-intro">
          Simple note taking app, part of a Serverless POC.
        </p>
        <Notes />
      </div>
    );
  }
}

export default App;
